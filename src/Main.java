public class Main {
    public static void main(String[] args) {

        new Square().draw();
        System.out.println();

        new Rectangle().draw();
        System.out.println();

        new Triangle().draw();
    }
}