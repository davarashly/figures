import java.util.Random;

public class Triangle extends Figure {
//    int a, b = 1, c;
//            while (((Math.sqrt(a * a + b * b)) % 1 != 0)) {
//        b++;
//    }
//    c = (int) Math.sqrt(a * a + b * b);

    Triangle() {
        rnd = new Random();
        int min = 10, max = 15;
        a = rnd.nextInt((max - min) + 1) + min;
        title = "Треугольник";
    }

    void draw() {
        System.out.println("\n" + title + "\n");
        for (int i = 0; i < a; i++) {
            boolean flag = false;
            for (int j = 0; j < i; j++) {
                if (j == 0) {
                    flag = true;
                    System.out.print("*");
                } else if (j == i - 1) {
                    flag = true;
                    System.out.print("*");
                } else {
                    flag = true;
                    System.out.print("*");
                }
            }
            if (flag)
                System.out.println();
        }
        for (int i = 0; i < a; i++) {
            System.out.print("*");
        }
    }
}
