import java.util.Random;

public abstract class Figure {
    int a;
    Random rnd;
    String title;

    abstract void draw();
}
